# GitLab Content Strategy 

Audit and update this document at the end of each quarter. 

## Introduction 

GitLab is strongly recognized as a code hosting and distributed version control
service. Recently recognized as a top performer in the Continuous Integration 
Forrester Wave, GitLab is starting to become recognized as a continuous integration
solution. 

Our current user base is mostly **X** who use GitLab for **X** and our current
customer base is mostly **X** who use GitLab for **X**. 

### Goals 

We want to change the perception that GitLab is “just a code repo” or “just a 
GitHub clone.” Our goal is to generate broad awareness of GitLab as a single 
application for the whole software development and operations lifecycle. While 
we want to continue to serve the developer community (and GitLab contributor 
community at large) we aim to better serve and attract IT practitioners and 
software development professionals. They need a way to support their transformation 
into a modern software company and appeal to their current and future developers. 
GitLab is uniquely positioned to do so as the application includes all of the tools 
needed by developers and operations. No integration necessary. 

### Audience 

GitLab's primary audience is full stack web developers who work for SMBs in the 
computer hardware/services/software SaaS industry. 

![image](/content/images/job-function.png)
![image](/content/images/company-size.png)
![image](/content/images/industry.png)

About 22% of our audience works for large businesses (1,000+ developers). The majority 
of our audience (93%) identify as male, 71% are between 25 and 40 years old, and the
majority are based in Europe and Russia. The majority of our audience work in an
office. 

![image](/content/images/gender-identity.png)
![image](/content/images/age.png)
![image](/content/images/location.png)

Read more detailed descriptions of GitLab's personas [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/4ab03ef26bf96668d9359339041739782b24d8ba/doc/development/ux_guide/users.md).

#### Software developers

**Add more description once the focus group has completed.**

#### IT practioners   

**Need a content-driven focus group**

## Analysis

### Existing content 

Currently, GitLab’s content is ad hoc, general (i.e. not targeted), and relatively 
void of personality (i.e. safe). We have a good catalog of GitLab branded images 
to use in blog posts, on social, and in webcast decks, but because they are 
illustrations, they lack relevance and emotional appeal toward our audiences. Most 
of our content is product-focused and thus out-of-date because our product changes 
rapidly. 

While we currently have a blog, it is mostly used for company communications 
(announcements, product releases) 

### Content gaps 

We have large gaps in our content. While we have some content covering continuous 
integration, cloud native, git, and DevOps, most of it resides offline either 
gated or on YouTube. 

## Core strategy 

GitLab's core content stategy can be broken down into three parts. Every piece
of content created appeal at least one of these categories. There will be times 
when content appeals to all three. 

- **Empathy** Pathos. First and foremost our content is empathetic and should always 
demonstrate a clear understanding of the needs and feelings of our audience. 
- **Community** Ethos. Our content should uphold the GitLab motto that "everyone can contribute"
and encourage collaboration, contribution, and information sharing. 
- **Authority** Logos.  Our content should always be well researched, cited, and factual. 

### Editorial Mission Statement 

Empower and inspire DevOps teams to collaborate better, 
be more productive, and ship faster by sharing insightful and actionable 
information, advice, and resources. 

### Vision 

Build the largest and most diverse community of co-conspirators who are leading
the way to definte and create the next generation of software development practices.

### 2018 Core Content Strategy Statement 

The content we produce helps increase awareness of GitLab's single application for 
the whole software development and operations lifecycle with the goal of broadening 
our market share and increasing sales by providing informative and persuasive content
that makes development and operations teams realize the benefits of transforming
into a software company with an effective DevOps culture, workflow, and tools. 

### Themes 

To reach the IT practitioner audience and continue to serve developers, GitLab will concentrate on the following themes (in order of priority):

- **Continuous everything: The path to modernization.** Covering continuous 
integration, delivery, deployment, and the relationship between the three 
concepts, expanding on this theme will help our audiences understand how to 
implement and execute on the core  principles needed to achieve development 
speed efficiently and safely while establishing GitLab as an authority on the 
topic. Content under this theme can be explainers, tutorials or how-tos, 
examples, and technology guides. 
- **DevOps: The people behind the software.** This theme focuses on the importance 
of uniting the work of development and operations. It covers the culture, best 
practices, tips, and tools to improve collaboration between dev and ops. Content 
under this theme should focus on the people 
- **Learn Git: Reducing the barrier to entry.**
- **Open source: The future of software development.** Open source software is  This theme covers the benefits of open source, 
- **Agile:** Iteration over perfection. 

| Theme | Developers | IT Practioners |
|---|---|---|
| Continuous everything | text explaining developer view point | text explaining IT practioner view point |
| DevOps | text explaining developer view point | text explaining IT practioner view point |
| Learn Git | text explaining developer view point | text explaining IT practioner view point |
| Open Source | text explaining developer view point | text explaining IT practioner view point |
| Agile | text explaining developer view point | text explaining IT practioner view point |

## Content Plan 

For the first quarter of 2018, we will work on implementing the editorial style 
guide, tone of voice, and brand personality as well as integrating the core strategy
into our onsite content.

### Blog

The [GitLab blog](https://about.gitlab.com/blog/) is a critical medium of communication
with our developer and open source community as well as our IT leadership and decision-making
audience. Because these audiences respond to and look for different types of content
from us (technical articles and tutorials vs thought leadership), in 2018 we will
experiment with separating content based on intended audience, reserving technical, 
developer-focused content and culture/brand stories for the blog, and publishing 
high-level, thought leadership on another channel (Medium or LinkedIn). 

### Webcast program

#### Bi-weekly I2P GitLab EEP product demos 

Can be offered as part of onboarding to trial users/new users ("Welcome" campaign, 
investigation/consideration-stage buyers)

**Purpose:** Awareness of GitLab as an end-to-end platform for software development & delivery 

**Audience:** trials, new users of GitLab, investigation-stage buyers 

**Hosted by:** Solutions Architects 

**Topic:** Intro to GitLab EE I2P demo 

#### Deep-Dive Series 

Marketed broadly via general newsletter and webcasts newsletter segment. Can be 
offered on-demand to specific segments based on ConvDev index, in-app offers. 
Added to YouTube and `Resources` page.

**Purpose:** Product awareness, training, and expansion 

**Audience:** CE, GitLab.com users 

**Hosted by:** Product managers 

#### Quarterly thought leadership

**Purpose:** Broad brand awareness of GitLab

**Audience:** IT decision makers 

**Hosted by:** Sid (or) Mark P. (or) Job + guest 

**Topics:** Open Source/Core, Auto DevOps, Cloud Native, Continuous Delivery, ConvDev

### Customer use cases 

Customer explaining how they solved a problem, how GitLab helped

**Purpose:** lead generation, sales enablment 

**Audience:** IT decision makers / consideration stage buyers

**Hosted by:** Content team + customer

### social

#### youtube
YouTube is a democratically-managed hub for all demos, presentations, functional group updates, trainings and talks by GitLab team members. It provides a comprehensive picture of our development process, offering, and employee experience.

#### linkedin
LinkedIn is the home of our employer brand. It is the place for original (i.e., natively published) thought leadership content, and posts from other channels about the future of work, remote work at scale, and "first-person" accounts of the policies & solutions we pioneer every day as the largest remote-only organization in the world. 

#### slideshare
Slideshare is a new channel for us, in which we can promote presentations that we believe have long term value.

#### twitter
Twitter is our most robust and engaged channel, and our best place to both talk & listen to developers. We tweet GitLab blog posts, demos, docs, and third-party content that we think will be interesting and useful to developers, our core constituency. We also create social events (twitter polls, chats/AMAs, retro/kickoff live streams, etc.) that may or may not aim to convert followers to take some other action off-Twitter. Some of these events have no other purpose than to better understand, engage, and delight our fans. 

#### facebook
Facebook is a channel in which to further share a selection of our blog & Twitter content that we think is relevant and interesting for developers. Reach & engagement are both much lower on Facebook than Twitter.

#### google
Google+ is a channel in which to promote long-term content assets that are important for SEO. 