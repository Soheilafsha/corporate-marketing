<details>
<summary>How to use this epic code</summary>

- Open a new tab and head to https://gitlab.com/groups/gitlab-com/-/epics/new
- Important that all child epics are at the same or lower level of project. Eg. if the parent epic is in marketing, simply make child epics there as well.
- Copy all of the info below `</details>`
- Paste it into the new epic draft box
- Click "create epic"

</details>

`All items in code are to be filled out.`

### FY2`1H1`: Organic Social GTM Motion-`x`

##### Points of Contact

| Team | Contact |
| ------ | ------ |
| Social Lead | `insert handle` |
| Content Lead| `insert handle` |
| Brand Design Lead | `insert handle` |
| Campaign Manager | `insert handle` |

##### [GTM Motion Organic Social Calendar](null.com)
_(Pending creation by the social team. Available to select team members on this campaign only.)_

Social team member - be sure to share the content calendar link ([here's how to create it](https://support.sproutsocial.com/hc/en-us/articles/360042108512-Link-Sharing))

----- 

### Campaign Manager, please fill out the below section

#### 👥 Intended Audience + Details
<!-- helps the team write unique copy, but audiences are not targetable on organic channels -->

`Insert intended audience here. Add ancillary details that may help us to understand the target further.`

#### What is the story behind the campaign? Give us the "plain English" understanding of the objectives and what the user gets out of it.

`insert narrative summary here` 

#### Summary of any important dates (estimate if not exact)

`insert summary of important dates here`

#### Campaign Details Needed
- Campaign UTM: `insert here`
- Finance code: `insert here`
- Slack channel for working group: `insert here`
- Agenda for ongoing meetings: `insert here`

<details>
<summary>Admin to create epic correctly</summary>

#### To Do
- [ ] Name is this epic: Social GTM [campaign name] Epic
- [ ] Add this child epic to the GTM parent event for all things related to this GTM

#### Once this epic is opened, please open one issue for each individual piece of content that needs to be promoted, [with this template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-gtm-organic.md)
- Eg. the campaign has a blog, a webinar registration landing page, and other content. The blog, webinar registration, and other pieces should have their own issue.
- Attach the open issues to this social child epic

Be sure to:
- name and identify each issue

</details>

/label ~"Social Media" ~"Corp Comms" ~"Corporate Marketing" ~"usecase-gtm" ~"Stage::Awareness"
