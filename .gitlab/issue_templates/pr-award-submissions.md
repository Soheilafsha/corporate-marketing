*Note: Please flag all upcoming awards 6 weeks prior to the application deadline to allow enough time for content development and approvals from the appropriate internal team members.*

## 🏆 Award Description + Background
Add any benefits of securing the award, submission fee, as well as GitLab's history (years we submitted, won, were finalists, etc.)

## 📄 Qualifications for Entry + Submission Components

## 📔 Recommendation
Add specific recommendation: which category, what specifically we should submit (customer, etc)

## 📚 Resources

### SUBMISSION DEADLINE: 

❗️[Offline application form]❗️

------
#### Timeline and Approvals:
Factor in 2 weeks for internal reviews and 4 days for legal review.
* [ ] Corp Comms/PR team DRI @akulks 
* [ ] Internal stakeholder review/input (product, talent brand, etc) 
* [ ] Legal Review

## 📣 Promotion
 * [ ] @social to create approved social content for team to share via Bambu 

/label ~"corporate marketing" ~"Corp Comms" ~"PR" ~"PR awards" ~"mktg-status::plan"

/cc @akulks @cweaver1 @kpdoespr

/confidential
















