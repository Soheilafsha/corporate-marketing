## Vendor Reference Request

Vendor's GitLab DRI please fill in all relevant sections above the solid line. Some details may not be applicable. Please title the issue `Vendor Reference Request: XYZ Company`. Once the request has been submitted to the corp comms team, please allow for 7-10 days for review and approval of the request. The corp comms team will review all content and relevant info to ensure it follows the [SAFE framework](https://about.gitlab.com/handbook/legal/safe-framework/) and route through the approval process. 

### Vendor Reference Criteria

Please ensure that the vendor meets the below criteria before proceeding with the approval process. If you have any questions on the below criteria or need clarification please reach out to Corp Comms via the #external-comms channel on Slack.

* GitLab team and/or team member(s) need to have been using the product, service and/or technology for at **LEAST 6 months** prior to the reference request.
* The product, service, and/or technology has met or exceeded your expectations.
* E-group member approval for providing reference (E-group member will be for the specific organization using the vendor. For example, if it is a tool the security team uses, Eric Johnson will be the approver for e-group.)
* If the request involves GitLab logo/brand use, please ensure the vendor follows the below:         
  - [Trademark Guidelines](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/trademark-guidelines/) 
  - [Brand Standards](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-guidelines/#brand-standards) 

### Vendor Information for Review and Approvals 

* How are you using the vendor/technology? 

* Who is the GitLab DRI for the vendor?

* Why did you select this vendor/technology over others? 

* How well has the vendor/technology met your expectations and requirements? 

* What is the specific reference request? 

* What assets/content would the vendor like to develop for the reference? (case study, blog post, press release, GitLab logo on their website, etc.) If assets/content have been developed, please attach or include links below to the documents. 

* How will these assets be used externally? Marketing collateral? Website? Media Relations? 

### Approver List:

* [ ] Vendor DRI
* [ ] Corporate Communications
* [ ] E-group member for organization using the vendor
* [ ] Legal ([Legal Materials Review Process](https://about.gitlab.com/handbook/legal/materials-legal-review-process/))


###  Link related issues and epics
   - [ ] Please link to all related issues and the epic

------

/label ~"corporate marketing" ~"Corp Comms" ~"PR" 

/cc @nwoods1 @cweaver1 @kpdoespr

/confidential
