<!-- Title
Developer Evangelism: #EveryoneCanContribute cafe insights - MONTH 2021
-->

## :construction_site: Resources

- [Handbook](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/#everyonecancontribute-cafe)
- [Calendar](https://everyonecancontribute.com/page/events/)
- [Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp1Gni9SyudMmXmBJIp7rIc)

## :bulb: Sessions
<!-- Update the sections below with the session title and valuable insights, e.g. URLs to blog post, recording, social. -->
### 2021-XX-XX: 

### 2021-XX-XX: 

### 2021-XX-XX: 

### 2021-XX-XX: 



<!-- Please leave cc and assignees -->
/cc @gitlab-de

/assign @dnsmichi 

<!-- Please leave the label below on this issue -->
/label ~"Corporate Marketing" ~"dev-evangelism" ~"de-community" 
