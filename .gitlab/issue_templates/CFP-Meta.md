<!-- Hey, Thank you for taking the time to create an issue for your talk, this helps us coordinate submissions and have a central place for submissions to a CFP -->

<!-- Please use the format below to add an Issue title. Dates should be using ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines -->

<!-- CFP: <Event Name>, <Location>, <Event Dates> - Due: <CFP Due Date> -->

## Event Details

* `Name of Event`: 
* `Event Website`:
* `Event Dates`: 
* `Location`: 
* `CFP Closes`: 
* `CFP Notification Date`:  
* `CFP Submission Link`: 
* `GitLab Event Issue/Epic`: <!-- Link to Issue if available -->


## Co-ordination


### Submissions


| Speaker(s)  | Topic   | Link to submission draft (where available) | Submission Status |
|-------------|---------|--------------------------------------------|-------------------|
|             |         |                                            |                   |


## DE Checklist

* [ ] Add `CFP:Upcoming` label if no due date is planned and not open yet.
* [ ] Add `CFP-EDU` & `CFP-OSS` if CFP is for an Education or Open source related event respectively.
* [ ] Internally Promoted
* [ ] Speaker Sourcing
* [ ] [Community Reachout](https://gitter.im/gitlab/heroes?at=5fa2a6fa8a236947ba85474a) 



FYI: @gitlab-de

FYI: @jrachel1 for Coordination with Heroes.

/assign @abuango

<!-- Please leave the label below on this issue -->

/label ~Events ~Speaker ~SPIFF ~"mktg-status::plan" ~"dev-evangelism" ~"CFP" ~"CFP::Open" ~"DE-Status::Doing" ~"DE-Type::Content"

<!-- Please leave the label below on this issue -->
