**STEP ONE:** Title this request: Rebranding Request - [Item]

## Overview

Soon GitLab will be revealing a refresh on our branding. The public facing reveal will be at the end of April 2022 and in efforts to ensure that our brand refresh is respresented on all external facing channels and/or assets we would like you to submit any needed rebranding efforts using this issue template.

You might be wondering, do I need to actually rebrand the assets I'm using? The answer is `YES`. Our brand refresh includes updating our font, type usage and weights, colors, illustration style, and more...In order for all of our communications and brand awareness efforts to feel cohesive and whole rebranding will be needed across the org.

In full transparency, we won't be able to get to everything by the end of April, however, we are going to try our best. We will be using the info you provide below to prioritize all requests. Please answer truthfully

#### Where does the current asset live?
`(Link) to the current asset or location, if applicable`
`(Link) to original issue, if applicable`
`How is this currently being used?`

#### Was this originally created internally or by an agency/vendor?
* [ ] Internal
* [ ] Agency/Vendor
* [ ] I don't know

#### On a scale of low, medium, high, what is the priority of this rebrand request? Why?
* [ ] low
* [ ] medium
* [ ] high
* `WHY? Please give details of its current usage.`

#### Is there a specific due date this is needed by? _Note: public reveal of the refreshed branding won't be until the end of April_
`MM-DD-YYYY`

#### DRIs and Review
Who is the DRI for this project and who else needs to review for approval?

|Name|Role|
|------|------|
| `@yourhandle` | Requestor |
| `@user2` | `ROLE` |
| `@user3` | `ROLE` |

#### Other notes...
`Add any other notes around the request`

## For Brand Team Use ONLY

## Review Status
* [ ] Approved
* [ ] Reviewed with comment (comment in issue)




/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing" ~"rebrand"

/assign @amittner @kbolton1
