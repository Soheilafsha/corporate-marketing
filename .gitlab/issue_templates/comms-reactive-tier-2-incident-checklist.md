This issue template is to help guide the communications team through various tasks to assist in a reactive `Tier 2 Incident`. Not every item will be appropriate to each situation. Please add the DRIs for each Checklist item if they are incorrect or need to be assigned. **If you are listed here as a DRI - please take care of the item you're assigned within the appropriate window of time for that section and check off the item yourself. Please use the comments below for any necessary details or conversation.**

## 📰 The latest on the situation
`202X Mo Day - ADD FIRST SHORT SUMMARY OF THE SITUATION. PROVIDE LINKS. ADD ADDITIONAL UPDATES IN THIS FORMAT ABOVE THIS ONE (IN REVERSE CHRONOLOGICAL ORDER) AS WE ENCOUNTER MORE CHANGES`

-------

### ➡️ Step 1: Learn about the landscape by outlining interests in the topic from outside of GitLab
##### DRIs: @social, community operations, and communications teams
- On core social media channels (mainly Twitter, but also Facebook &LinkedIn), on niche community channels (HackerNews, Reddit, others), interest from the media, or from team members or non-team members in issue/MR comments or in Slack
- **Add updates on the situation to this issue, not Slack, unless immediate attention is necessary in Slack. This way, we keep everyone involved centralized in the issue and continue to have the latest info available for our senior leaders in one location**

- [ ] Inform the original slack threads that alerted the team to the situation by linking to this issue and asking that any updates be put here vs Slack.

### ➡️ Step 2: Determine if we need to pause organic outbound messages
##### DRIs: @social, community operations, and communications teams
- [ ] If **yes**, open the  **Step 2b** dropdown box below and follow the Checklist. Then continue onto **Step 3**.
- [ ] If **no**, skip **Step 2b** and delete it from the issue description. Then head to **Step 3**.

<details>
<summary>Step 2b checklist</summary>

### ➡️ Step 2b - every item is optional

#### Social Media Team (@Social)
###### Temporary organic social pause (Brand only)
- [ ] Pause all brand social posts by going to [Sprout Social settings > Publishing Settings](https://app.sproutsocial.com/settings/publishing/) and click `Stop messages`
- [ ] Head to [natively scheduled tweets](https://studio.twitter.com/schedule), and delete them
- [ ] Optional: Recommend that all Team Members pause company-focused posts in the #whats-happening-at-GitLab channel 
###### Ensure monitoring is in place for brand mentions and someone on the team is monitoring live (social + community coordinate)
- [ ] Confirm that the "Smart Inbox" in Sprout is cleaned and reflective of net-new mentions
- [ ] Pause all on-post engagements, likes, etc.
- [ ] Create an incident specific inbound message tag in Sprout: `Incident: Specific Topic`, make a note in the incident specific slack channel
- [ ] Review the next 1-2 weeks of scheduled posts for newly problematic topics or copy, make copy edits, and make a note in the incident specific slack channel
###### Share the immediate details of the organic social pause only (does not require the incident team yet)
- [ ] Share these details in both the #social_media and #marketing Slack channels, be sure to mention that details are forthcoming
- [ ] If there are any time sensitive social posts that were paused, inform the stakeholders either in the issue where it was requested or in their team Slack channel, tell them to join the public #slack-channel created in the list above
##### Optional Social Media To-Do's (if personalities and indirect GitLab channels need to pause)
- [ ] Inform content/editorial teams of social pause in the #content Slack channel and share a message mentioning @-contentteam
- [ ] Inform TEs to halt all company-focused posts by going to the #tech-evangelism Slack channel and share a message mentioning @-techevangelism
- [ ] Inform all executives and company personalities to halt all company-focused posts by going to the #e-group Slack channel and share a message mentioning @-exec-group

#### Communications Team
##### Preliminary recommendations for non-organic social channels (if necessary)
- [ ] Draft recommendations to be shared with Growth + O/B Demand Gen Teams; work with this new incident team on recommendations to send to CMO
   - These recommendations should include insight on what pausing looks like for other non-organic social channels. This is an optional item.
###### Set up internal communication channels
- [ ] When there is a plan to review with CMO: Open one private Slack channel for top-level comms with the comms team, growth, O/B Dem Gen, legal, executive leadership, and other relevant leaders
   - *It's critical that the action team be as small as possible and DRIs for all areas are accounted for. [This is not a consensus driven plan](https://about.gitlab.com/handbook/values/#collaboration-is-not-consensus).*

#### Growth 
- [ ] Inform Growth Marketing team in #growth-marketing Slack channel of the pause
- [ ] Review upcoming changes to the website for any negative impact, including, but not limited to:
   - [ ] Homepage hero
   - [ ] Blog posts
   - [ ] Video content

#### Community Relations team
- [ ] Identify the community response channels where the discussions around this incident is taking place. Generally, they will be Hackernews, Twitter, GitLab.com or Reddit, but conversations can also happen outside of our traditional channels (e.g. discussions on an external media article).
- [ ] Work with Developer Evangelists, Social Team, Community Operations, and any other pertinent parties to establish monitoring responsibilities
- [ ] Identify a set of experts for expert responses if necessary
- [ ] Create an incident-$INCIDENT_NAME tag and macro. Create a Zendesk trigger for auto tagging related mentions based on specific keywords. Tagging the tickets will be useful for the retrospective report after the incident has been closed.
- [ ] Use the #community-relations Slack channel to discuss specific responses, and the incident channel to discuss and keep aligned on response strategy and to escalate relevant community input to the rest of the incident team.

</details>

### ➡️ Step 3: Communication stakeholders deliberate on a response (typically within 48 hours of alert)
##### DRIs: External communications, internal communications, legal, and e-group (some folks outside of this list may also be involved)

- Fill out [incident response template](https://docs.google.com/document/d/1zqk9qxVUUYENRl2Hbnwp3j_x-CXQbpJCtYvl46fzVIQ/edit#) for information gathering and action plan creation and then `LINK GOOGLE DOC RESPONSE STRATEGY HERE`. 

<details>
<summary>Internal Communications</summary> 

##### DRI: [TBD]; currently legal team
- [ ] Optional: Prepare an internal statement for all staff from leadership. 
   - Acknowledge the same points made in the public posts. This statement should be written as if it were going to be seen publicly. 
   - If possible, have the leader film their response on webcam, post to GitLab Unfiltered YouTube, and make the video private.

</details>

<details>
<summary>External Communications</summary> 

##### DRI: @nwoods1
- Agree on an approved stance and language for any external message - from the brand or from executive (remember that shorter and simpler is better) 
- If a text-only statement, put text on a plain background image; if sharing on social, we will still need social copy for the post
- If posted on social, publish posts and pin to top of channels (leave pinned for as long as the environment remains the same)

</details>

### ➡️ Step 4: Provide external communication through approved channels; continue to monitor
- DRIs: Channel owners (publishing message); Monitoring team already in place
Publish the approved asset to the agreed upon channels. For social, do not engage with folks further than this statement.
- Everyone monitoring external channels is responsibile for Code of Conduct enforcement. This is for both GitLab channels like the website (issues, MRs) and for social channels. If a user is acting outside of GitLab Code of Conduct and/or outside of the specific-channel terms of service, take the native reporting action available.


### ➡️ Step 5: Get feedback and adapt future messaging 

<details>
<summary>(When the incident seems resolved) Open a restrospective issue</summary> 

##### DRIs: @wspillane
- [ ] [Create a confidential retrospective issue using the retrospective-incident template in the corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/d53146b8da9a9460375c1ea7a4eafa7fa5fa4af3/.gitlab/issue_templates/retrospective-incident.md) and tag all folks involved in this incident in the comments, assign to a milestone and due date approx 1 week away. Unassign everyone in this issue, and mark the retrospective issue to close this one when resolved.

</details>

<details>
<summary>Adapt future messaging</summary> 

##### DRIs: @social
- [ ] Review the next 1-2 weeks of scheduled posts for newly problematic topics or copy, make copy edits, and make a note in the incident specific slack channel

</details>

-------------

<!-- Leave the section below for issue tags
/label ~"Social Media" ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"
/assign @social @nwoods1
/confidential
 -->
