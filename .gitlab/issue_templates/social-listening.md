<!--
Please choose one of the following to add to the topic of the issue description 
## 🟢 This Listening topic is `ACTIVE` and will continue until `INSERT DATE`
## 🟢 This Listening topic is `INACTIVE` and is scheduled to run from `INSERT START DATE` to `INSERT END DATE`.
-->

## Background
This issue is to manage the listening setup necessary to drive listening reports around `INSERT TOPIC NAME`.

## What we're listening for
[define why we're using social listening for this campaign. Be clear so that others can identify who listening is an important element.] 

## Sources
[list all sources in the listening builder here] 

## Facebook Configuration
Facebook listening requires selecting specific pages to listen to. Please work with Comms & other teams to identify potential 3rd parties that would talk about this topic and add them to the Facebook specific query bulder. 

[add screenshot of Sprout Facebook Listening configuration OR list 'none at this time']

## Listening Queries
Latest as of `INSERT DATE` -- additional queries may be added at a later date in the event we identify other ways users are talking about this topic.

[add screenshot of Sprout Query Builder]

## Exclusions 
[add a screenshot of Sprout Exclusions OR list 'none at this time']

## Optional Filters
[add a screenshot of Sprout Optional Fliters OR list 'none at this time']

/milestone %"Organic Social: Triage"

/assign @social


/confidential
