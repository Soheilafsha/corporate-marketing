* **Please rename the issue as Conference Template_FYXX QX: 20XX Event Name - Location, Date**

## :notepad_spiral: Event Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **CMM:** 
* **CMC:**
* **Tactic/Event Type:** Select the [event type](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#conference)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event 
* **Official Event Name:** 
* **Event Type:**
   - [ ] In-Person
   - [ ] Hybrid
   - [ ] Virtual
* **Date:**
* **Location:**
* **Slack Channel for Planning:**
* **Slack Channel for Event:**
* **Time Zone for Event:**  
* **Event Website:**
* **Internal Deliverables Doc (master tracker for CCM & team):**
* **If we sponsored last year, link to SFDC campaign:** 
* **Allocadia ID:**
* **Campaign Tag:** 
* **Classification ID for Travel Expenses:** 
* **Goals:**
   - [ ] Lead Goal:
   - [ ] MQL Goal (FM provides goal): 
* **Show Messaging (booth/collateral/ancillary):** 
* **Overall Budget:** 
* **Region (APAC/AMER/EMEA):**  
* **Key Stakeholders Involved:**
   - [ ] Field Marketing
   - [ ] Channels & Alliances Marketing
   - [ ] Demand Generation

## Event Demographics
(FMM to provide a description to attendees)
* **Demographics:** 
* **Technical/Non-Technical:** 
* **Focus/Theme:** 
* **Recommended messaging for target audience:** 
* **Recommended staff for event (technical, security, sales):** 

## :sleuth_or_spy: Description of Event
* **About:** 
* **No. of Attendees:** 
* **No. of Sponsors:** 
* **Attending partners/competitors:** 

## :level_slider: Sponsorship Level & Details 
* **Sponsorship Level:** 
* **Thought Leadership:**
* **Exhibit Size/no. demo stations/theater:**
* **Meeting Room:**
* **Branding:**
* **Advertising:**

## :thought_balloon: : Thought Leadership
* **Keynotes, Sessions, Workshops, Lightning Talks:** 
* **Speaker:**
* **Title:**
* **Bio (link):**
* **Photo:**
* **Date/Time of Speaking Engagement:**
* **Session Title:**
* **Abstract:**

## :notebook_with_decorative_cover: GitLab Exhibit: _in-person event only_
**(Link to show floor plan)
* **Hours:** 
* **Install:**
* **Exhibit Hours:**
* **High Traffic Times:**
* **Teardown:**
* **No. Demo Stations:**
* **Demo Type:**
* **Signage Per Station:**
* **Theater:**
* **No. Presentations:**
* **Giveaways:** 
* **Collateral:** 
* **Proof of Image of Booth:**   

## :white_check_mark: Ancillary Events
* **Name:**
* **Partner (if applicable):**
* **Location:**
* **Date:**
* **Type of event (dinner, happy hour, party, reception):**
* **Capacity:**
* **Registration Link:**
* **Promotional Plan:** 

## :busts_in_silhouette: Staff List
Please read through the [Event Handbook page](https://about.gitlab.com/handbook/marketing/events/#employee-booth-guidelines) for best practices at events. Once you commit to an event, please make sure to plan to attend.  
| Name              | Title    | Team | Pass Type |
* **Support Team: CMM or CMC Onsite:**
* **Registration (Link or Code):** 
* **Hotel:**   

## :trophy:  Outreach and Follow-Up: Outreach/Promotion of Event: Social Media/Email Campaign/Direct Mailer
* **Social Campaign:**
* **Email Campaign:**
* **Other:**

* What is the lead follow-up process? 
Will we be asking the attendees who they want to have follow up with them? Will our SDRs do the follow-up and then assign the opp to the channel? etc.	 

## :performing_arts: Booth/Theatre
- [ ]  Positioning and Messaging confirmed
- [ ]  Booth slide deck created
- [ ]  Click through demo set up (iPad): link 

### Lead scanning _in-person event only_
- [ ]  Lead Scanner Vendor: 
- [ ]  Link to website to access Leads 
* **No. of Scanners:**
* **Login/PW:**
* **Lead license assigned:**

## :hourglass_flowing_sand: Deliverables
* **Master Tracker for show deliverables linked:** 

## :airplane: Travel _in-person event only_
Make sure you have manager approval and [COVID committee approval](https://about.gitlab.com/handbook/travel/#policy-and-guidelines) to attend
   - [ ] **Travel Dates:**
   - [ ] **Link to hotel block or recommendation (if applicable):**
   - [ ] **DEADLINE to book your travel:**

## :package: Swag and Event Assets _in-person event only_

### Shipping to Event  
* **Shipping Address:**   
* **Point of Contact:**  
* **Deadline for Shipments:**  
* **Items Ordered/Quantities/Vendor** (example: 100 notebooks from Nadel):  
* **Tracking Number:**    
* [ ] **Items Received**  

### Return Shipping
* **Onsite DRI Responsible for Return Shipping:**  
* **Nadel return shipping label provided?** YES/NO  
*   **If no:**  
	* **Address for Return:**  
	* **Fedex Account Number:** Available in marketing 1pass if needed
* **Return Tracking Number** (Provided by Onsite Event DRI):  
- [ ] **Items Shipped Back**  

## :iphone: Event Promotion
- [ ] Social issue created using this [issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-event-request.md)
- [ ] LinkedIn campaign setup

## :two_women_holding_hands: Channel Partner Section 
* **This section must be filled out if you are running this campaign with a channel partner.**

- [ ] Is the partner inviting us to speak onstage or at the booth theater? YES/NO
- [ ] Are we sponsoring an ancillary partner event? YES/NO  
- [ ] Is the partner driving registration? YES/NO
- [ ] Is the partner standing up the landing page for registration? YES/NO
- [ ] Is the partner supplying graphics or printing for an event giveaway? YES/NO  


### Lead follow-up with Channel Partner 
- [ ] Partner is inviting us to speak onstage or at the booth theater
   - [ ] If yes, will GitLab receive leads at any point?   
   - [ ] Please describe how the partner plans to follow-up on leads received from this event/campaign: 

## :checkered_flag: Post Event
* [ ]  Lead list received from organizer _usually within 1 week post-event_
* [ ]  All pictures uploaded to Google Drive
* [ ]  List locked (all changes after lock to be made in SFDC) and leads uploaded to SFDC campaign 
* [ ]  Lead list cleaned and uploaded and add to nurture submitted
* [ ]  Follow up email triaged
* [ ]  Event recap deck created 
* [ ]  Post-Event feedback issue created for all on-site and planning staff

## :open_file_folder: Assignments and Labels
* /assign @eparker9086 @amandawshen
* `Assign the Quarter and Region tags to the issue`
* /label ~Corporate Marketing ~Corporate Event ~Sponsored Corporate Events ~Events ~mktg-status::wip
