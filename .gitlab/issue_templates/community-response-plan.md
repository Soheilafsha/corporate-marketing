# Community Response Plan for <ADD ANNOUNCEMENT/INCIDENT NAME HERE>

## Timing
* Date of announcement/incident: 

## Resources 
<!-- Include links to blog posts, FAQs, issues, a forum post, news storys, Hacker News threads, and other relevant resources. -->

* Resource 1 
* Resource 2 
* Resource 3 
* Etc. 

## Key concerns 
<!-- List the expected and/or expressed community concerns. -->

* Concern 1
* Concern 2 
* Concern 3 
* Etc. 

## Key messages
<!-- List the key messages we want to convey in our responses to the community. -->

* Message 1
* Message 2 
* Message 3
* Etc. 

## Coverage plan (all times PT)
<!-- Include the cadence of updates, Slack channel for updates, the dates and times of coverage shifts, and DRI for each shift. -->

* Update cadence: Every <X> hours 
* Date - Hours - DRI 
* Date - Hours - DRI 
* Date - Hours - DRI 

## Engaging experts
Our team cannot anticipate every question from the community. We will engage experts working on this situation in the Slack channel listed in the Coverage Plan section of this issue to address questions that we cannot be answer with the available resources.  

CC <enter stakeholder GitLab handles here>

## Possible Questions
<!-- Include the key community questions we expect which are not addressed in the existing resources and corresponding answers. -->

* Question  1
  * Answer 1
* Question  2
  * Answer 2
* Question  2
  * Answer 2
* Question N
  * Answer N

/confidential
/label ~"dev-evangelism" ~"Community response" ~"Community Relations"
/assign @johncoghlan 
