## :trophy:  Outreach and Follow Up ~"SDR" 
Note: This section is required to be filled out by the Corp DRI & SDR leadership for all owned events (either virtual or inperson) at least 30-days prior to the event. If it is not an owned event, then this section is optional. 

* SDR Manager to assign project lead: (Corp DRI assign to SDR Manager here)
* SDR DRI: (SDR manager to assign SDR here)

### Goals for SDR Team - (to be filled in by Corp Event DRI)
*SDR project lead, please review the full issue to get an understanding of the event. Please review the [handbook](/handbook/marketing/revenue-marketing/sdr/#event-promotion-and-follow-up-assistance) for more information on this process.*

- Pre-event SDR goal: 
  - [ ] XX number of registrations
  - [ ] Meetings at event
  - [ ] Other (add here)
- Post-event SDR goal:
  - [X] Follow up and SAOs
  - [ ] Other (add here)

- Targets:
  - Inclusion Job Titles (Seniority):
  - Inclusion Job Titles (Function):
  - Target number of attendees: 

### Pre Event ~"SDR"
* [ ] SDR Manager: enable team to begin compiling a list of prospects to reach out to. Each SDR should grab at least 15 prospects from a min of 10 accounts with a goal of 50 accounts covered. Completion date: **Corp DRI to assign.**
* [ ] SDR Project lead: clone the appropriate master sequence and add in event-specific information or create a more personalized sequence. Set up 30 minutes to review with SDR manager and Corp DRI. Make edits as necessary and determine go live date. Master sequence settings have an exact date and time. **Completion date: at least three weeks before event.**
* [ ] Send sequence URL to SDR manager to add to the Outreach collection `Events/Campaigns 2020`
* [ ] Announce sequence availability in slack channel for teams involved (sync with your manager if unsure what channels). All reps partaking should now begin sequencing their prospect lists as the sequence won't start until the exact date and time chose when setting it up. **Completion date: agreed upon go live date**

### Post Event for ~"SDR" 
##### Follow up: planned from Corp Events:
FMC to pull in link from `Follow-up email` MPM issue, so everyone is aligned on the follow-up message and the offer that is being made. Please delete this text and replace it with the link once the issue has been created. 

##### Follow up: needed from SDR and Sales team:
(FMC to edit this if follow up is needed from the SDR team and if specific information needs to be relayed. If this isn’t needed, please remove this section) Completion date: created and reviewed with management and FMM one week prior to event. 

/label ~SDR ~Events ~"Corporate Event" ~"Corporate Marketing" ~"mktg-status::WIP" ~"SDR::Event Awareness" 