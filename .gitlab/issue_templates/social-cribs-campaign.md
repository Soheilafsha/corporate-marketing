## Organic Social IG Submission for Cribs Campaign

_Each submission should get its own issue. These issues are intended to be attached to the Organic Social: Cribs Publication Issue. Other questions that are campaign-wide get answered in the FY22H1 Cribs Social Campaign Epic._

### 📬 STEP 1: Get your setup ready for your picture(s)!

- Take a moment to organize your setup
- Remove any personally identifying materials you don't want in the picture (paper bills, written passwords)
- Be aware of what else is in the shot. Are you okay with family pictures, personal trinkets, and other items in the photo?
- Consider leaving something interesting or items that you can't work without and include them in your set up list. Can't work with lip balm? Keep a bowl of peanuts on your desk? Do you have a stack of books? Show us and tell us why it's important to you.

##### You're ready to take your picture(s)! Please make these photos _horizontal_ (not square or vertical) by turning your phone (or using your camera) so that images are in a 16:9 aspect ratio (like HDTV) or close to this format. 

Please take photo(s) using the full resolution available on your device - so be sure to use your native camera app on smartphones. Image file sizes should be larger than 1 MB when uploaded.

---

### 📬 STEP 2: Answer a few questions that will help us write a caption for your post

##### 1.  What makes your work setup unique?

`add details here.`

---

##### 2.  What is the one item you cannot work without? Tell us why!

`add details here.`

---

##### 3.  Tell us just a bit about you

- Preferred first name: `add here`
- Job title: `add here`
- State, Country of residence: `add here`

- [ ] Yes, I'd like my Instagram account tagged in the image/caption. My handle is @`add here`

_Note, that if you agree to be tagged that the social team will continue to moderate comments, but we may tag you in comments if folks have a question about something in your picture._

---

### 🗓 STEP 3: Upload your images in the design tab below and follow the instructions for naming the items in your setup

- Drop the image in OR click upload designs to attach
- Write a comment over each item on your desk
   - Click the item. Write the description of that item. Then click comment.

##### [Here is an example of what your image and comments should look like.](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4401/designs/IMG_3462.jpg)

The social team may reach out to you about with questions about specifics, but we'll tag you in the image.



/label ~"Social Media" ~"Corp Comms" ~"Corporate Marketing" ~"mktg-status::plan" 

/assign @social

/milestone %"Organic Social: Triage"
