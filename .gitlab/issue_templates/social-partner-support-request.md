### Request for organic social promotion of partner marketing initiatives with a "Big 5" partner
<!-- If you are not a member of the partner marketing team and requesting brand social support, please use the social-general-request template. --> 

<!-- Requester please fill in all sections above the solid line. Some details may not be applicable. --> 
#### Requester: Please acknowledge the following before filling out your request
* [ ]  I understand that all social requests need a minimum of *2 full weeks* between the request and the first published post date and that I cannot put in a request for the same week. If less than 2 weeks and identified as critical, please note in the description along with why.
* [ ] I understand that if my request is not tied to a corporate marketing-focused timed event or campaign, that the publishing schedule is entirely at the discretion of the social team, though they'll make the effort to lock in schedule availability.
## 📬 STEP 1: For Partner Marketing Team Member <!-- Requester please fill in all **[sections]** above the solid line for STEP 1. Please do not open the issue until you're ready to answer the following questions. If the social team still needs more info, they'll ask in the comments. Note that these questions are critical for understanding your request. -->
**1.  What is your request all about?**

`add details here`

- [ ] AWS (add ~"Alliances - AWS" label)
- [ ] Google Cloud (add ~"Alliances - Google" label)
- [ ] Hashicorp (add ~"Alliances - Hashicorp" label)
- [ ] RedHat (add ~"Alliances - Red Hat & IBM" label)
- [ ] VMWare (add ~"Alliances - VMware" label)


**2.  Pertinent Dates**
- Content Live Date (when the webcast happens, blog is public, etc): `x/xx/xx` 
- Promotion by Date (when we'd ideally be promoting the content; in advance for events, after publication for blogs, etc.): `x/xx/xx`

**3. 🔗 Add appropriate link(s) for user journey; Consider required creative**
- [ ] None, this is a social only/first ask
- [ ] Yes, it's a GitLab owned link; `add link here`
- [ ] Yes, it's a 3rd party (non-GitLab) link; `add link here`

Is there existing creative to consider or will we need to create our own?
_Creative elements, like images or videos, are required for sharing on organic social channels. [Check links using this social card validator](https://cards-dev.twitter.com/validator). If there is a card, we won't need to create additional assets._

* [ ]  There is an existing social card/opengraph attached to the link
* [ ]  Existing assets from the brand design team are here: `insert link to related issue or repo for creative`
* [ ]  I require new custom assets for social  (no card attached to link, no assets currently available)
* [ ]  I'm not sure what I require (the social team will review with you in the comments below) 

**4.  What is the overall utm_campaign? - *webcast123*, i.e. *utm_campaign=webcast123***
<!-- If this is a GitLab owned link, GitLab team will decide what campaign terms to use. If the link is to a 3rd party partner site, please ask our partner about any UTM parameters they'd like us to use for their tracking. -->

`add utm_campaign URL here`   
* [ ] I'm not sure what my utm_campaign code is or there isn't one

**5.  What are the ideal objectives? (Think clicks, sign ups, awareness, etc.)**
* [ ]  I want people to know about the information (*impressions on social posts*)
* [ ]  I want people to respond, share info with others, and/or take action on-social channels (*engagements on social posts*)
* [ ]  I want people to register, read, or continue off-social media channels (*clicks to the link attached to the posts*)

**6. Are there existing assets to use on social or will you require custom designs?**  <!-- links with cards, images, or videos -->
* [ ]  Existing Assets (from the partner or from other campaign use) to use are located here: `insert link to location of assets`
* [ ]  I require custom assets for social (see question **6b** below)
* [ ]  I'm not sure what I require (the social team will review with you in the comments below)

**6b**. Do we have logo files for the partner and are allowed to use them in any social design? 
* [ ] No
* [ ] Yes; the transparent HQ logos are available here: `add link here` 

**7. Are you considering or will be adding paid social advertising for this campaign?** <!-- Note that paid social advertising is managed by the digital marketing programs team and will require a separate issue; having this info helps us better build funnels and synchronize work. -->
* [ ]  Yes, I'm requesting paid social advertising `add link to the paid social issue if available`
* [ ]  No, I am not requesting paid social advertising
* [ ]  I'm not sure (the social team will help you determine the right path)

**8.  Anything else we should know or could be helpful to know?**

`add additional thoughts here. Subjective things are helpful. If this is a new partner, a company we're looking to wow, using this effort to extend the relationship, etc.`

**9. Please link all related issues in the *related issues section below* 👇**

-----

## 📝 STEP 2: Social Team: To-Do's
<!-- Be sure there is enough detail with the info above. If an area is not pertinent (e.g. live coverage isn't necessary), please use a strikethrough <s> to <s> to cross out the text.-->
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please use Canva to accomplish. If this is tied to a larger campaign, please open a design issue, tag requester, and link as a related issue here.

## ✍️ STEP 3: All: Drafts, suggestions, and schedule 
*Please work on these items in the comments below.*

## 🗓 STEP 4: Social Team: Scheduled posts
* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Comment on this issue with any need-to-know details

## STEP 5 Partner Marketing Team Memeber: Please approve the suggested posts in the comments below 👇
* [ ]  I have approved the suggested copy for all posts discussed in the comments
*<!--  Note, we're not asking for stylistic approval, but on messaging and strategy. -->

## 📭 Social Team: Once the posts available to add to Sprout are scheduled and live coverage is completed, you may close this issue.

/label ~"Social Media" ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"

/assign @social

/milestone %"Organic Social: Triage"
